<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 07.01.2016
 * Time: 00:08
 */

namespace Communication\Utils;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;

class Message
{
    /**
     * RabbitMQ host
     * @var string
     */
    private $_host = "46.101.88.91";

    /**
     * RabbitMQ queue
     * @var string
     */
    protected $_queue;

    /**
     * @var AMQPChannel
     */
    protected $_channel;

    /**
     * RabbitMQ username
     * @var string
     */
    private $_user = "guest";

    /**
     * RabbitMQ password
     * @var string
     */
    private $_password = "guest";

    /**
     * RabbitMQ port 
     * @var int
     */
    private $_port = 5672;

    /**
     * @var AMQPStreamConnection
     */
    protected $_connection;

    public function __construct($queue)
    {
        $this->_queue = $queue;
        $this->_connection = new AMQPStreamConnection($this->_host, $this->_port, $this->_user, $this->_password);
        $this->_channel = $this->_connection->channel();
    }

    protected function close()
    {
        $this->_channel->close();
        $this->_connection->close();
    }
}