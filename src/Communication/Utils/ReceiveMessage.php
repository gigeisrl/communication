<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 07.01.2016
 * Time: 00:20
 */

namespace Communication\Utils;


class ReceiveMessage extends Message
{
    public function getChannel(){
        return $this->_channel;
    }

    public function getConnection(){
        return $this->_connection;
    }
}