<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 06.01.2016
 * Time: 23:51
 */

namespace Communication\Utils;

use PhpAmqpLib\Message\AMQPMessage;

class SendMessage extends Message
{
    /**
     * The that will be send to rabbitMQ
     * @var AMQPMessage
     */
    private $_message;

    public function setMessage($object)
    {
        $this->_message = new AMQPMessage(serialize($object),
            array('delivery_mode' => 2)
        );
    }

    public function send()
    {
        $this->_channel->basic_publish($this->_message, '', $this->_queue);
        $this->close();
    }
}