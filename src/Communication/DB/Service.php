<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 07.01.2016
 * Time: 21:32
 */

namespace Communication\DB;


class Service extends MysqliDb
{

    protected $host = "localhost";

    protected $username = "root";

    protected $password = "";

    protected $db = "service";

    protected $port= 3306;

    public function __construct()
    {
        parent::__construct($this->host,$this->username,$this->password,$this->db,$this->port);
    }
}