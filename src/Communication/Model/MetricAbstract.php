<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 13.01.2016
 * Time: 11:01
 */

namespace Communication\Model;

use Communication\Utils\SendMessage;

abstract class MetricAbstract
{
    protected $_queue;
    protected $_table;

    public function send(){
        $message = new SendMessage($this->_queue);
        $message->setMessage($this);
        $message->send();
    }

    abstract public function saveToDB();
}