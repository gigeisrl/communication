<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 13.01.2016
 * Time: 11:08
 */

namespace Communication\Model;


class Article extends MetricAbstract
{
    protected $_queue = "articles";

    protected $_table = "articles";

    private $_site_id;

    private $_name;

    private $_cat_id;

    private $_tags;

    private $_content;

    private $_action;

    /**
     * @var \Communication\DB\Service $_db
     */
    private $_db;
    public function __construct($site_id, $name = "",$cat_id = 0,$tags = "",$content ="")
    {
        $this->_site_id = $site_id;
        $this->_name = $name;
        $this->_cat_id = $cat_id;
        $this->_tags = $tags;
        $this->_content = $content;
    }

    public function saveToDB(){
      $this->_db  = new \Communication\DB\Service();

        $data = Array ("site_id" => $this->_site_id,
            "name" => $this->_name,
            "cat_id" => $this->_cat_id,
            "tags" => $this->_tags,
            'content' => $this->_content
        );
        $this->{$this->_action}($data);
    }

    private function insert($data){
        $id = $this->_db->insert($this->_table, $data);
    }

    private function update($data){

        $this->_db->where('site_id',$this->_site_id);
        $this->_db->get($this->_table);
        if($this->_db->count == 0){
            $this->insert($data);
        }else {
            $this->_db->where('site_id',$this->_site_id);
            $this->_db->update($this->_table, $data);
        }
    }

    private function delete($data){
        $this->_db->where('site_id',$this->_site_id);
        $this->_db->delete($this->_table);
    }

    public function setAction($action = "insert"){
        $this->_action = $action;
    }

}