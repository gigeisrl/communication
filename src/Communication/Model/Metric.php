<?php
/**
 * Created by PhpStorm.
 * User: personal
 * Date: 07.01.2016
 * Time: 22:49
 */

namespace Communication\Model;

class Metric extends MetricAbstract
{
    private $_hash;

    private $_article;

    public function __construct($article)
    {
        $this->_hash = $_COOKIE['hash'];
        $this->_article = $article;
    }

    public function saveToDB(){
        $db = new \Communication\DB\Service();

        $data = Array ("hash" => $this->_hash,
            "article" => $this->_article,
        );
        $id = $db->insert ($this->_table, $data);;
    }
}